<?php

namespace ingenstream\helper;

use InvalidArgumentException;

class ParameterFilter
{
    /**
     * 过滤整数类型参数
     *
     * @param mixed $value
     *
     * @return int|null 过滤后的整数值，如果无法转换为整数则返回 null
     */
    public static function filterInt(mixed $value): ?int
    {
        if (is_numeric($value)) {
            return (int)$value;
        }
        return null;
    }

    /**
     * 过滤字符串类型参数
     *
     * @param mixed $value
     *
     * @return string|null 过滤后的字符串值，如果无法转换为字符串则返回 null
     */
    public static function filterString(mixed $value): ?string
    {
        if (is_string($value)) {
            return trim($value);
        }
        return null;
    }

    /**
     * 过滤布尔类型参数
     *
     * @param mixed $value
     *
     * @return bool 过滤后的布尔值
     */
    public static function filterBool(mixed $value): bool
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * 参数过滤处理
     *
     * @param array|object $params
     * @param array        $rules 【'输入key','默认值','过滤值','重命名key'】
     *
     * @return array
     */
    public static function filterParams(array|object $params, array $rules): array
    {
        $params = is_object($params) ? (array)$params : $params;//兼容数组对象参数
        /** @var TYPE_NAME $filteredParams */
        $filteredParams = [];
        foreach ($rules as $rule) {
            $inputKey     = $rule[0] ?? null;
            $defaultValue = $rule[1] ?? null;
            $filterValue  = $rule[2] ?? null;
            $replaceKey   = $rule[3] ?? null;
            if (empty($inputKey)) {
                continue;
            }
            //优先传入参数
            if (array_key_exists($inputKey, $params)) {
                $paramValue = $params[$inputKey];
            } else {
                $paramValue = $defaultValue;
            }
            // 参数值过滤
            if (isset($filterValue) && !empty($filterValue)) {
                switch ($filterValue) {
                    case 'string':
                        $paramValue = (string)$paramValue;
                        break;
                    case 'int':
                        $paramValue = (int)$paramValue;
                        break;
                    case 'float':
                        $paramValue = (float)$paramValue;
                        break;
                    case 'bool':
                        $paramValue = filter_var($paramValue, FILTER_VALIDATE_BOOLEAN);
                        break;
                    default:
                        // 自定义过滤器函数
                        if (is_callable($filterValue)) {
                            $paramValue = call_user_func($filterValue, $paramValue);
                        } else {
                            throw new InvalidArgumentException("Invalid filter specified for param {$inputKey}.");
                        }
                }
            }
            // 替换参数键名（可选）
            $outputKey                  = $replaceKey ?? $inputKey;
            $filteredParams[$outputKey] = $paramValue;
        }
        return $filteredParams;
    }

}

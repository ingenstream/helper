<?php

namespace ingenstream\helper;

use RulerZ\Compiler\Compiler;
use RulerZ\RulerZ;
use RulerZ\Target\Native\Native;

/**
 *
 * 表达式验证
 * @author Mr.April
 * @since  1.0
 */
class Expression
{
    public static function eval(string $expr, object|array $args): bool
    {
        try {
            // 创建一个RulerZ编译器
            $compiler = Compiler::create();

            // 创建RulerZ实例
            $rulerZ = new RulerZ($compiler, [new Native(['length' => 'strlen'])]);

            // 应用规则到对象
            $result = $rulerZ->satisfies($args, $expr);
            return $result ?? false;
        } catch (\Exception $e) {
            return false;
        }
    }

}
